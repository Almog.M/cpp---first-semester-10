#include "Customer.h"
#include <set>

Customer::Customer(string name)
{
	this->_name = name;
	this->_items = DEFAULT_ITEMS_SET;
}

Customer::Customer()
{
	this->_name = DEFAULT_CUSTOMER_NAME;
	this->_items = DEFAULT_ITEMS_SET;
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double totalSum = 0;
	set<Item>::iterator curr;
	bool addedLastItmeToCart = false;

	//For calcs the sum.
	Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

	//Calcs the total sum of the cart.
	//If the set do not find an item it returns an iterator to the end of the set.
	for (int i = 0; i < NUM_OF_ITEMS_TYPES_IN_STORE; i++)
	{
		curr = this->_items.find(itemsList[i]);
		if (curr != this->_items.end()) totalSum += curr->totalPrice();
	}

	return totalSum;
}

void Customer::addItem(Item newItem)
{
	//A pair that we can know with him about if we added a new item or one that we added in the past.
	pair <set<Item>::iterator, bool> knowladgePair = this->_items.insert(newItem); //Returns an iterator to the element that in the set (that
	//We want to add), and boolean for if the item has added to the set.
	
	if (!knowladgePair.second) //The item is existed in the set.
	{
		set<Item>::iterator itemIt = knowladgePair.first; //Iterator to the item that we want to add the amount to.
		int currAmount = itemIt->getCount();
		((Item&)itemIt.operator*()).setCount(currAmount + 1); //Adding one product to the item type.
		//The same - ((Item*)(itemIt.operator->()))->setCount(currAmount + 1);
	}

}

void Customer::removeItem(Item itemToRemove)
{
	set<Item>::iterator it = this->_items.find(itemToRemove);
	if (it != this->_items.end())
	{
		((Item&)(it.operator*())).setCount(it->getCount() - 1);
		if (it->getCount() == 0) //Do not have to the buyer any product of this item.
		{
			this->_items.erase(itemToRemove);
		}
	}
}

void Customer::setName(string name)
{
	this->_name = name;
}

void Customer::setItems(set<Item> newItems)
{
	this->_items = newItems;
}

set<Item>& Customer::getItems()
{
	return this->_items;
}

string Customer::getName()
{
	return this->_name;
}
