#pragma once
#include"Item.h"
#include<set>

#define NUM_OF_ITEMS_TYPES_IN_STORE 10
#define DEFAULT_CUSTOMER_NAME "Someone"
#define DEFAULT_ITEMS_SET {} //An empty one.


class Customer
{
public:
	Customer(string name);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item newItem);//add item to the set
	void removeItem(Item itemToRemove);//remove item from the set

	//get and set functions
	void setName(string name);
	void setItems(set<Item> newItems);

	set<Item>& getItems();
	string getName();

private:
	string _name;
	set<Item> _items;


};
