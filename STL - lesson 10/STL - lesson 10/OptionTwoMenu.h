#pragma once
//#include "MainMenu.h"
#include <iostream>
#include <map>
#include "Customer.h"
#include "Item.h"
#include <string>
#include <exception>

//class MainMenu;

#define OPTION_TWO_MENU_ADD_TO_CART_OPTION 1
#define OPTION_TWO_MENU_REMOVE_FROM_CART_OPTION 2
#define OPTION_TWO_MENU_EXIT 3
class OptionTwoMenu
{
private:
	int _userOption;
public:
	void printMenu();
	void setOption(int option);
	void doOption(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE], string buyerName);
	void addItems(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE], string buyerName);
	void removeItems(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE], string buyerName); //Remove items in a loop.
	OptionTwoMenu(int option);
	OptionTwoMenu();
	~OptionTwoMenu();
};

