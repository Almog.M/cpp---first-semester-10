#include "Item.h"

Item::Item(string name, string serialNumber, double unitPrice)
{
	this->setName(name);
	this->setSerialNumber(serialNumber);
	this->setUnitPrice(unitPrice);
	this->setCount(1);
}

Item::~Item()
{
	
}

double Item::totalPrice() const
{
	return _unitPrice * _count;
}

bool Item::operator<(const Item & other) const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator==(const Item & other) const
{
	return this->_serialNumber == other._serialNumber;;
}

string Item::getName()
{
	return this->_name;
}

void Item::setName(string name)
{
	this->_name = name;
}

int Item::getCount() const
{
	return this->_count;
}

void Item::setCount(int count)
{
	this->_count = count;
}

string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

void Item::setSerialNumber(string serialNumber)
{
	this->_serialNumber = serialNumber;
}

double Item::getUnitPrice()
{
	return this->_unitPrice;
}

void Item::setUnitPrice(double newPrcie)
{
	this->_unitPrice = newPrcie;
}
