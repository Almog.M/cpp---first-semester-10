#include "OptionTwoMenu.h"
#include "MainMenu.h"


OptionTwoMenu::OptionTwoMenu()
{
	this->_userOption = NON_OPTION;
}

void OptionTwoMenu::printMenu()
{
	std::cout << "1 --> Add to your cart." << "\n";
	std::cout << "2 --> Remove from your cart." << "\n";
	std::cout << "3 --> Return to the main menu" << "\n\n";
}

void OptionTwoMenu::setOption(int option)
{
	this->_userOption = option;
}

void OptionTwoMenu::doOption(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE], string buyerName)
{
	switch (this->_userOption)
	{
		case OPTION_TWO_MENU_ADD_TO_CART_OPTION:
			this->addItems(abcCustomers, itemsList, buyerName);
			break;
		case OPTION_TWO_MENU_REMOVE_FROM_CART_OPTION:
			this->removeItems(abcCustomers, itemsList, buyerName);
			break;
		case OPTION_TWO_MENU_EXIT:
			break;
		default: 
			throw exception("Invalid choice");
	}
}

void OptionTwoMenu::addItems(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE], string buyerName)
{
	MainMenu mainM; //For using in functions in.
	map<string, Customer>::iterator customerIt;


	customerIt = abcCustomers.find(buyerName);

	if (customerIt == abcCustomers.end())
		throw exception("The buyer is a new one - can't do this one...");
	else //This is a old buyer!!!
	{
		mainM.addToCart(buyerName, abcCustomers, itemsList);
	}

}

void OptionTwoMenu::removeItems(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE], string buyerName)
{
	MainMenu mainM;
	map<string, Customer>::iterator customerIt;
	int itemOption = 0;
	customerIt = abcCustomers.find(buyerName);

	if (customerIt == abcCustomers.end())
		throw exception("The buyer is a new one - can't do this one...");
	else //This is a old buyer!!!
	{
		do
		{
			std::cout << "What would you like to remove from your cart?" << "\n";
			mainM.getItemsM().printMenu();
			std::cin >> itemOption;
			getchar(); //Cleans the buffer.

			if (itemOption != ITEMS_MENU_EXIT)
			{
				if (itemOption >= 1 && itemOption <= 10) //Valid option in this case (not include 0).
				{
					customerIt = (map<string, Customer>::iterator)abcCustomers.find(buyerName); //For canceling the const iterator.
					customerIt->second.removeItem(itemsList[itemOption - 1]);
				}
				else
				{
					throw exception("Invalid option.");
				}
			}

		} while (itemOption != 0);
	}

}

OptionTwoMenu::OptionTwoMenu(int option)
{
	this->_userOption = option;
}

OptionTwoMenu::~OptionTwoMenu()
{
}
