#include "ItemsMenu.h"
#include "MainMenu.h"

class MainMenu;

ItemsMenu::ItemsMenu()
{
	this->_userOption = NON_OPTION;
}


void ItemsMenu::printMenu()
{
	std::cout << "0 --> Stop to buy" << "\n";
	std::cout << "1 --> product: Milk, price: 5.3" << "\n";
	std::cout << "2 --> product: Cookies, price: 12.6" << "\n";
	std::cout << "3 --> product: bread, price: 8.9" << "\n";
	std::cout << "4 --> product: chocolate, price: 7.0" << "\n";
	std::cout << "5 --> product: cheese, price: 15.3" << "\n";
	std::cout << "6 --> product: rice, price: 6.2" << "\n";
	std::cout << "7 --> product: fish, price: 31.65" << "\n";
	std::cout << "8 --> product: chicken, price: 25.99" << "\n";
	std::cout << "9 --> product: cucumber, price: 1.21" << "\n";
	std::cout << "10 --> product: tomato, price: 2.32" << "\n";
	std::cout << "\n";
}

ItemsMenu::ItemsMenu(int option)
{
	this->_userOption = option;
}

ItemsMenu::~ItemsMenu()
{
}
