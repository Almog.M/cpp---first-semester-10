#pragma once
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

#define DEFAULT_NAME "None"
#define DEFAULT_SERIAL_NUMBER "00000"
#define DEFAULT_UNIT_PRICE 0.1


class Item
{
public:
	Item(string name, string serialNumber, double unitPrice);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions
	string getName();
	void setName(string name);

	int getCount() const;
	void setCount(int count);

	string getSerialNumber() const;
	void setSerialNumber(string serialNumber);

	double getUnitPrice();
	void setUnitPrice(double newPrcie);

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};