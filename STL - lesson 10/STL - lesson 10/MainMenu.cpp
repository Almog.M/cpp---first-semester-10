#include "MainMenu.h"



void MainMenu::newBuyer(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE])
{
	string buyerName = DEFAULT_CUSTOMER_NAME;
	map<string, Customer>::iterator customerIt;

	std::cout << "Enter your name please." << "\n";
	std::cin >> buyerName;
	getchar(); //Cleans the buffer.

	customerIt = abcCustomers.find(buyerName);

	if (customerIt != abcCustomers.end())
		throw exception("The buyer is already existed...");
	else //This is a new buyer!!!
	{
		addToCart(buyerName, abcCustomers, itemsList);
	}


}

void MainMenu::printCustomerInfo(string buyerName, map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE])
{
	map<string, Customer>::iterator customerIt;
	set<Item> customerSet;
	set<Item>::iterator setIt;
	double totalPrice = 0;


	customerIt = abcCustomers.find(buyerName);

	if (customerIt != abcCustomers.end()) //If the customer is a buyer.
	{
		customerSet = customerIt->second.getItems();
		totalPrice = customerIt->second.totalSum();

		for (int i = 0; i < NUM_OF_ITEMS_TYPES_IN_STORE; i++)
		{
			setIt = customerSet.find(itemsList[i]);
			if (setIt != customerSet.end()) //If the item is in the customer set (shopping cart).
			{
				std::cout << "Item name: " <<((Item&)(*setIt)).getName() << std::endl;
				std::cout << "Item serial number: " <<((Item&)(*setIt)).getSerialNumber() << std::endl;
				std::cout << "Item unit price: " <<((Item&)(*setIt)).getUnitPrice() << std::endl;
				std::cout << "Item amount: " <<((Item&)(*setIt)).getCount() << std::endl;
				std::cout << "\n";
			}
		}
		
		std::cout << "Your (" << buyerName << ") total price is: " << totalPrice << "\n\n";
	}
	else
		throw exception("You are not a buyer here...");

}

void MainMenu::addToCart(string buyerName, map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE])
{
	int itemOption = 0;
	map<string, Customer>::iterator customerIt = abcCustomers.find(buyerName);
	do
	{
		std::cout << "What would you like to buy?" << "\n";
		this->_itemsM.printMenu();
		std::cin >> itemOption;
		getchar(); //Cleans the buffer.

		if (itemOption != ITEMS_MENU_EXIT)
		{
			if (itemOption >= 1 && itemOption <= 10) //Valid option in this case (not include 0).
			{
				abcCustomers.insert(pair<string, Customer>(buyerName, Customer(buyerName))); //Inserting the buyer - if has added wont add again.
				customerIt = (map<string, Customer>::iterator)abcCustomers.find(buyerName); //For canceling the const iterator.
				customerIt->second.addItem(itemsList[itemOption - 1]);
			}
			else
			{
				throw exception("Invalid option. If you did not add any item, the buyer wont be in the customers stock");
			}
		}

	} while (itemOption != 0);


}

void MainMenu::modifyShoppingCart(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE])
{
	map <string, Customer>::iterator customerIt;
	string buyerName = "";
	int option = 0; //As default.
	std::cout << "Enter your name" << "\n";
	std::cin >> buyerName;
	getchar();
	customerIt = abcCustomers.find(buyerName);
	if (customerIt == abcCustomers.end()) throw exception("You did not buy here yet...\n");//The buyer did not buy.
	else
	{
		this->printCustomerInfo(buyerName, abcCustomers, itemsList);
		do
		{
			std::cout << "Enter an option: " << "\n";
			this->_option2M.printMenu();
			std::cin >> option;
			getchar();
			this->_option2M.setOption(option);
			this->_option2M.doOption(abcCustomers, itemsList, buyerName);

		}
		while (option != OPTION_TWO_MENU_EXIT);
	}
}

string MainMenu::maxPriceBuyer(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE])
{
	string maxName = DEFAULT_CUSTOMER_NAME; //About price, not dictionary way.
	map<string, Customer>::iterator customerIt;
	double maxTotalPrcie = 0;
	double currTotalPrice = 0;

	if (!abcCustomers.empty())
	{
		customerIt = abcCustomers.begin();
		while (customerIt != abcCustomers.end())
		{
			currTotalPrice = customerIt->second.totalSum();
			if (currTotalPrice > maxTotalPrcie)
			{
				maxTotalPrcie = currTotalPrice;
				maxName = customerIt->first;
			}
			customerIt++;
		}
	}
	else
		throw exception("The customers stock is empty...Do not have max.");


	return maxName;
}

int MainMenu::getOption() const
{
	return this->_userOption;
}

void MainMenu::setOption(int option)
{
	this->_userOption = option;
}

ItemsMenu & MainMenu::getItemsM()
{
	return this->_itemsM;
}

void MainMenu::printMenu()
{
	std::cout << "0 --> print customer items list (includes prices)." << "\n";
	std::cout << "1 --> to sign as customer and buy items" << std::endl;
	std::cout << "2 --> to update existing customer's items" << std::endl;
	std::cout << "3 --> to print the customer who pays the most" << std::endl;
	std::cout << "4 --> to exit" << std::endl;
	std::cout << "\n";
}

void MainMenu::doOption(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE])
{
	string buyerName;
	map<string, Customer>::iterator maxBuyerIterator;
	switch (this->_userOption)
	{
		case PRINT_CUSTOMER_ITEMS_LIST:
			std::cout << "Enter your name: " << "\n";
			std::cin >> buyerName;
			getchar();
			this->printCustomerInfo(buyerName, abcCustomers, itemsList);
			break;
		case NEW_BUYER_OPTION:
			this->newBuyer(abcCustomers, itemsList);
			break;
		case MODIFY_SHOPPING_CART_OPTION:
			this->modifyShoppingCart(abcCustomers, itemsList);
			break;
		case MAX_BUYER_INFO_OPTION:
			buyerName = this->maxPriceBuyer(abcCustomers, itemsList); //Gives name.
			this->printCustomerInfo(buyerName, abcCustomers, itemsList); //About name and total sum.
			break;
		case MAIN_MENU_EXIT:
			std::cout << "Have a good day, bye buy (;\n" << std::endl;
			break;
		default:
			throw exception("Invalid choice");
			break;
	}
}

MainMenu::MainMenu() : _itemsM(ItemsMenu(NON_OPTION)), _option2M(OptionTwoMenu(NON_OPTION))
{
	_userOption = NON_OPTION;
}

MainMenu::~MainMenu()
{
}
