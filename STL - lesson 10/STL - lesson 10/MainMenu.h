#pragma once
#include <iostream>
#include <map>
#include "Customer.h"
#include "Item.h"
#include <string>
#include "ItemsMenu.h"
#include "OptionTwoMenu.h"
#include <exception>

#define MAIN_MENU_EXIT 4
#define NON_OPTION -1
#define PRINT_CUSTOMER_ITEMS_LIST 0
#define NEW_BUYER_OPTION 1 //A buyer wants to do a buy in the shop.
#define MODIFY_SHOPPING_CART_OPTION 2 //A buyer that has buy something, can modify his\her cart.
#define MAX_BUYER_INFO_OPTION 3 //Gives info about the buyer that has bought in the max price (overall).(Max buyer is the buyer that has payed the biggest amount...)

//class OptionTwoMenu;
//class ItemsMenu;

class MainMenu
{
private:
	int _userOption;
	ItemsMenu _itemsM;
	OptionTwoMenu _option2M;

	//Functions.
	void newBuyer(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE]);
	void printCustomerInfo(string buyerName, map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE]);
	//Adds to a customer's cart until he/her says stop.
	void modifyShoppingCart(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE]); //For known buyer.
	string maxPriceBuyer(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE]); //Gives name.
public:
	void addToCart(string buyerName, map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE]);

	//Getters and setters:
	int getOption() const;
	void setOption(int option);

	ItemsMenu& getItemsM();

	void doOption(map<string, Customer>& abcCustomers, Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE]);
	void printMenu();
	MainMenu();
	~MainMenu();
};

