#include"Customer.h"
#include "Item.h"
#include<map>
#include <iostream>
#include <utility>
#include "MainMenu.h"
#include <set>
#include <algorithm>
#include <exception>


#define NUM_OF_ITEMS_TYPES_IN_STORE 10



int main()
{
	//M for menu.
	MainMenu mainM;
	int generalOption = 0; //For receiving from the user an option...
	map<string, Customer> abcCustomers;
	Item itemsList[NUM_OF_ITEMS_TYPES_IN_STORE] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	//Start to work!!!
	do
	{
		std::cout << "Welcome to MagshiMart!\nEnter an option: " << "\n";
		mainM.printMenu();
		std::cin >> generalOption;
		getchar(); //Cleans the buffer.
		mainM.setOption(generalOption);
		try
		{
			mainM.doOption(abcCustomers, itemsList); //Doing one of the options in the menu according to the option that has sent.
		}
		catch (exception& e)
		{
			std::cout << e.what();
			std::cout << "\n\n";
		}
		
	} while (mainM.getOption() != MAIN_MENU_EXIT);

	getchar();
	return 0;
}